from matplotlib import pyplot as plt
def suivant(Xn,a,c,m):
    return (a*Xn + c)%m
def valeurs(X0,a,c,m,N):
    tabX = []
    for i in range(N):
        tabX.append(X0)
        X0 = suivant(X0,a,c,m)
    return tabX

print(valeurs(0,3,5,136,20))

#Les termes sont periodiques du coup pas aleatoires

V = valeurs(0,75,74,(2**16)+1,2**20)
plt.plot(V,'b')
plt.show()

V1 = valeurs(123489,16807,0,(2**31)-1,10000)
V3 = valeurs(25743,16807,0,(2**31)-1,10000)
plt.scatter(V1, V3, s=.2)


V4 = valeurs(5,16807,0,(2**31)-1,10000)
V5 = valeurs(6,16807,0,(2**31)-1,10000)
plt.scatter(V4, V5, s=.2)


V6 = valeurs(123488,16807,0,(2**31)-1,10000)
V7 = valeurs(123489,16807,0,(2**31)-1,10000)
plt.scatter(V6, V7, s=.2)

#Pour les petites graines on peut facilement voir que c'est periodique
#Mais pour les graines ca semble bien aleatoire

class Generateur:
    def __init__(self,a,c,m):
        self.a = a
        self.c = c
        self.m = m
        self.x = None
    
    def graine(self,g):
       self.x = g
      
    def aleatoire(self):
        if(self.x == None):
            raise ValueError("générateur non initialisé !")
        return (self.a*self.x+self.c)%self.m
    
    
g = Generateur(3,5,17)
g.aleatoire()

g.graine(5)
print(g.aleatoire())

        

        