from random import * 
import time
from matplotlib import pyplot as plt
def eltMajDet(T):
	nbrOcc = 0
	n = len(T)
	for i in range(n):
		for j in range(n):
			if(T[i] == T[j]):
				nbrOcc = nbrOcc+1
		if(nbrOcc > n):
			return T[i]
        

def eltMajProba(T):
    nbrOcc = 0
    n = len(T)
    while(nbrOcc<n/2):
        x = choice(T)
        for i in range(n):
            if(x == T[i]):
                nbrOcc =nbrOcc+1
    return x


def tabAlea(n, a, b, k):
    if(k<n/2):
        return "k doit etre superieur ou egale a n/2"
    tab = []
    m = randint(a,b)
    for i in range(k):
        tab.append(m)
    for i in range(n-k):
        y = randint(a,b)
        while(y == m):
            y = randint(a,b)
        tab.append(y)
    shuffle(tab)
    return tab


def tabDeb(n, a, b, k):
    if(k<n/2):
        return "k doit etre superieur ou egale a n/2"
    tab = []
    m = randint(a,b)
    for i in range(k):
        tab.append(m)
    for i in range(n-k):
        y = randint(a,b)
        while(y == m):
            y = randint(a,b)
        tab.append(y)
    shuffle(tab)
    tab[0] = m
    return tab

def tabFin(n, a, b, k):
    if(k<n/2):
        return "k doit etre superieur ou egale a n/2"
    tab = []
    m = randint(a,b)
    for i in range(k):
        tab.append(m)
    for i in range(n-k):
        y = randint(a,b)
        while(y == m):
            y = randint(a,b)
        tab.append(y)
    shuffle(tab)
    pos = randint(k,n)
    tab[pos] = m
    return tab

tab1 = tabAlea(10000,1,20,5001)
tabDebA = tabDeb(10000,1,20,5000) 
tabFinA = tabFin(10000,1,20,5000)

print(eltMajDet(tab1))
print(eltMajDet(tabDebA))
print(eltMajDet(tabFinA))

print(eltMajProba(tab1))
print(eltMajProba(tabDebA))
print(eltMajProba(tabFinA))

def contientEltMaj(T,m):
    M = []
    n = len(T)
    cpt = 0
    for i in range(m):
        randomNum = randint(0,n)
        M.append(randomNum)
    for j in range(m):
        for i in range(n):
            if(M[j] ==  T[i]):
                cpt = cpt+1
                if(cpt>=n/2):
                    return True
    return False

def testContient(n,a,b,k,N,m):
    reussite = 0 
    tab = tabAlea(n,a,b,k)
    for i in range(N):
        if(contientEltMaj(tab,m)):
            reussite = reussite + 1
    return reussite

propReus = []
k = 500
while(k<=1000):
    propReus.append(testContient(1000,1,20,k,1000,1)) 
    k = k + 50
plt.plot(propReus) # Ligne brisée
plt.show()   

propReusM = []
m = 1
while(m<=50):
    propReusM.append(testContient(1000,1,20,500,1000,m))
    m = m+1
plt.plot(propReusM)
plt.show()
        
    















