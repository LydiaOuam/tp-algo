from random import *
from matplotlib import pyplot as plt

def entierAleatoires(n,a,b):
    liste = []
    for i in range(n):
        x=randint(a,b)
        liste.append(x)
    return liste

def entiersAleatoires2(n,a,b):
	liste =[]
	for i in range(n):
		liste.append(randrange(a,b))
	return liste

L1 = []
L2 = []
L1 = entierAleatoires(1000,1,100)
L2 = entiersAleatoires2(1000,1,100)

plt.hist(L1, bins=100) # histogramme avec 100 colonnes
plt.show() 

plt.hist(L2,bins = 100)
plt.show()


def flottantsAleatoires(n):
	liste = []
	for i in range(n):
		liste.append(random())
	return liste

def flottantsAleatoires2(n,a,b):
	liste = []
	for i in range(n):
		liste.append(uniform(a,b))
	return liste
        

R1 = flottantsAleatoires(1000)
R2 = flottantsAleatoires2(1000,-3,10)

plt.plot(R1) # Ligne brisée
plt.show()

plt.plot(R2) # Ligne brisée
plt.show()


def pointsDisque(n):
	cpt = 0
	liste = []
	points = []
	while(cpt<n):
		liste = flottantsAleatoires2(2,-1,1)
		if(liste[0]**2+liste[1]**2 <= 1):
			points.append(liste)
			cpt = cpt + 1
	return points



def pointsDisque2(n):
    points = []
    for i in range(10):
        cpt = 0
        x = uniform(-1,1)
        while(cpt<n):
            y = uniform(-1,1)
            if(x**2+y**2<=1):
                points.append([x,y])
                cpt = cpt+1
    return points



def affichagePoints(L):
	X = [x for x,y in L] # on récupère les abscisses...
	Y = [y for x,y in L] # ... et les ordonnées
	plt.scatter(X, Y, s = 1) # taille des points minimale
	plt.axis('square') # même échelle en abscisse et ordonnée
	plt.show()
    
    
    
P1 = []
P2 = []
P1 = pointsDisque(10000)
affichagePoints(P1)

P2 = pointsDisque2(100)
affichagePoints(P2)

def aleatoireModulo(N):
	x = getrandbits(10)
	return x%N


def aleatoireRejet(N):
	x = getrandbits(10)
	while(x>=N):
		x = getrandbits(10) 
	return x

E1 = [] 
E2 = []
for i in range(10000):
	E1.append(aleatoireModulo(100))
	E2.append(aleatoireRejet(100))

plt.hist(E1, bins=100) # histogramme avec 100 colonnes

plt.show() # Affichage du résultat

plt.hist(E2,bins=100)
plt.show()











    