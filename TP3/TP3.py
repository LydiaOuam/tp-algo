import math
import random
import dessins
def distance(A,B):
    d = (B[0] - A[0])**2 + (B[1] - A[1])**2
    d = math.sqrt(d)
    return d

A, B, C = (121,77), (48,70), (12,72)
print(distance(A,B), distance(A,C), distance(B,C))

def aretes(P):
    distances = []
    n = len(P)
    for i in range(n):
        j = i+1
        while(j<n):
            d = distance(P[i],P[j])
            distances.append((i,j,d))
            j = j +1
    return distances

P = [(6,20),(67,18),(96,4),(32,45)]
print(aretes(P))
            
def pointsAleatoires(n, xmax, ymax):
    liste = []
    for i in range(n):
        r1 = random.randint(0,xmax)
        x = r1*random.random()
        r2 = random.randint(0,ymax)
        y = r2*random.random()
        liste.append((x,y))
    return liste

point = pointsAleatoires(3, 5, 10)
#dessins.dessinPoints(point)

def listesAdjacence(n, A):
    listeadjacence = {}
    for i in range(n):
        adjacence = []
        for point in A:
            if(point[0] == i):
                adjacence.append(point[1])
            if(point[1] == i):
                adjacence.append(point[0])
        listeadjacence[i] = adjacence
    return listeadjacence

A = [(0,1),(0,2),(0,3),(1,2),(1,3),(2,3)]
adj = listesAdjacence(4, A)
print(adj)
dessins.dessinGraphe(A,adj)
